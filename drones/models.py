from django.db import models
from django.core.validators import MinValueValidator, MaxValueValidator, RegexValidator


class Drone(models.Model):
    """This class represents a Drone Entity model."""
    IDLE = 'IDLE'
    LOADING = 'LOADING'
    LOADED = 'LOADED'
    DELIVERING = 'DELIVERING'
    DELIVERED = 'DELIVERED'
    RETURNING = 'RETURNING'
    STATE = [
        (IDLE, 'IDLE'),
        (LOADING, 'LOADING'),
        (DELIVERING, 'DELIVERING'),
        (DELIVERED, 'DELIVERED'),
        (RETURNING, 'RETURNING'),
    ]

    class Model(models.TextChoices):
        LIGHTWEIGHT = 'LI', "Lightweight"
        MIDDLEWEIGHT = 'MI', "Middleweight"
        CRUISERWEIGHT = 'CR', "Cruiserweight"
        HEAVYWEIGHT = 'HE', "Heavyweight"

    serial_number = models.CharField(max_length=100, )
    model = models.CharField(max_length=2, choices=Model.choices, default=Model.LIGHTWEIGHT)
    weight_limit = models.IntegerField(validators=[MinValueValidator(1), MaxValueValidator(500)])
    battery_capacity = models.IntegerField(validators=[MinValueValidator(0), MaxValueValidator(100)])
    state = models.CharField(max_length=32, choices=STATE, default=IDLE, )

    class Meta:
        db_table = 'drone'
        verbose_name = 'Drone'
        verbose_name_plural = 'Drones'
        ordering = ['id']

    def __str__(self):
        return self.serial_number


class Medication(models.Model):
    """This class represents a Medication Entity model."""
    name = models.CharField(max_length=255, validators=[RegexValidator(r'^[A-Za-z0-9_-]+$')])
    weight = models.IntegerField()
    code = models.CharField(max_length=255, validators=[RegexValidator(r'^[A-Z0-9_]+$')])
    image = models.ImageField(upload_to='uploads/%Y/%m/%d/')
    drone = models.ManyToManyField(Drone, through='Load')

    class Meta:
        db_table = 'medication'
        verbose_name = 'Medication'
        verbose_name_plural = 'Medications'

    def __str__(self):
        return self.name


class Load(models.Model):
    drone = models.ForeignKey(Drone, on_delete=models.CASCADE)
    medication = models.ForeignKey(Medication, on_delete=models.CASCADE)
