from rest_framework.test import APITestCase
from rest_framework import status


class CreateDroneTestCase(APITestCase):
    def test_create_drone(self):
        data={"serial_number":1,"model":"MI","weight_limit":400,
              "battery_capacity":30,"state":"IDLE"}
        response = self.client.post("/api/drone/register/",data)
        self.assertEqual(response.status_code,status.HTTP_201_CREATED)
    def test_check_available_drone(self):
        response = self.client.get("/api/drone/available/")
        self.assertEqual(response.status_code,status.HTTP_200_OK)