from __future__ import absolute_import, unicode_literals
from celery import shared_task
from datetime import datetime
from .models import Drone
@shared_task(name = "print_msg_main")
def print_message(message, *args, **kwargs):
  print(f"Celery is working!! Message is {message}")

@shared_task(name = "check_drones_battery")
def check_drones_battery():
  drones = Drone.objects.all()
  for drone in drones:
    print(f"Battery level for {drone.serial_number} is {drone.battery_capacity}")
