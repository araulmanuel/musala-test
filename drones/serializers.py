from rest_framework.serializers import ModelSerializer
from .models import Drone, Medication, Load


class DroneSerializer(ModelSerializer):
    class Meta:
        model = Drone
        fields = '__all__'

class DroneOnlyBaterySerializer(ModelSerializer):
    class Meta:
        model = Drone
        fields = ('battery_capacity',)

class MedicationSerializer(ModelSerializer):
    class Meta:
        model = Medication
        fields = '__all__'


class LoadSerializer(ModelSerializer):
    class Meta:
        model = Load
        fields = '__all__'
