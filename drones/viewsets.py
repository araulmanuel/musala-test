from rest_framework.generics import CreateAPIView, ListAPIView, RetrieveAPIView
from rest_framework.response import Response
from rest_framework.views import APIView

from .models import Drone, Medication, Load
from .serializers import DroneSerializer, MedicationSerializer, LoadSerializer,DroneOnlyBaterySerializer
from rest_framework.parsers import FormParser, MultiPartParser
from rest_framework.filters import BaseFilterBackend
import coreapi


class SimpleFilterBackend(BaseFilterBackend):
    def get_schema_fields(self, view):
        return [coreapi.Field(
            name='drone',
            location='query',
            required=False,
            type='string'
        )]


class CreateDrone(CreateAPIView):
    queryset = Drone.objects.all()
    serializer_class = DroneSerializer



class AvailableDrone(ListAPIView):
    serializer_class = DroneSerializer

    def get_queryset(self):
        return Drone.objects.filter(weight_limit__gte=1, battery_capacity__gte=25)


class CheckDrone(RetrieveAPIView):
    queryset = Drone.objects.all()
    serializer_class = DroneOnlyBaterySerializer


class CreateMedication(CreateAPIView):
    queryset = Medication.objects.all()
    serializer_class = MedicationSerializer
    parser_classes = (FormParser, MultiPartParser)


class CheckingMedicationByDrone(ListAPIView):
    queryset = Medication.objects.all()
    serializer_class = MedicationSerializer
    parser_classes = (FormParser,)
    filter_backends = (SimpleFilterBackend,)

    def list(self, request, *args, **kwargs):
        drone_id = request.GET.get('drone')

        queryset = self.get_queryset()
        queryset = queryset.filter(load__drone=drone_id)
        serializer = MedicationSerializer(queryset, many=True)
        return Response(serializer.data)


class CreateLoad(CreateAPIView):
    queryset = Load.objects.all()
    serializer_class = LoadSerializer

    def create(self, request, *args, **kwargs):
        data = request.data
        medication = Medication.objects.get(id=data['medication'])
        drone = Drone.objects.get(id=data['drone'])
        if drone.weight_limit >= medication.weight and drone.battery_capacity > 25:
            drone.weight_limit -= medication.weight
            drone.state="LOADING"
            drone.save()
            load = Load.objects.create(drone=drone, medication=medication)
            load.save()
            serializer = LoadSerializer(load)
            return Response(serializer.data)
        else:
            return Response({'error': 'Drone is full capacity or battery level is below 25%'})

