"""musala URL Configuration

The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/3.2/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  path('', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  path('', Home.as_view(), name='home')
Including another URLconf
    1. Import the include() function: from django.urls import include, path
    2. Add a URL to urlpatterns:  path('blog/', include('blog.urls'))
"""
from django.contrib import admin
from django.urls import path, include
from drf_yasg.views import get_schema_view
from drf_yasg import openapi
from rest_framework import permissions
from django.conf import settings
from django.conf.urls.static import static
from drones.viewsets import CheckDrone,CreateDrone,CreateMedication,CreateLoad,CheckingMedicationByDrone,AvailableDrone
schema_view = get_schema_view(
    openapi.Info(
        title="Drone API",
        default_version='v1',
        description="Musala test",
        terms_of_service="https://www.google.com/policies/terms/",
        contact=openapi.Contact(email="araulmanuel@gmail.com"),
        license=openapi.License(name="BSD License"),
    ),
    public=True,
    permission_classes=(permissions.IsAuthenticatedOrReadOnly,),
)
urlpatterns = [
    path('api/doc/', schema_view.with_ui('swagger', cache_timeout=0)),
    path('api/redoc/', schema_view.with_ui('redoc', cache_timeout=0)),
    path('admin/', admin.site.urls),
    path('api/drone/register/', CreateDrone.as_view(),name='drone'),
    path('api/drone/load/', CreateLoad.as_view(),name='load'),
    path('api/drone/available/', AvailableDrone.as_view(),name='available'),
    path('api/drone/checkbatery/<int:pk>', CheckDrone.as_view(),name='checkbatery'),
    path('api/medication/create/', CreateMedication.as_view(),name='medication'),
    path('api/medication/checkbydrone/', CheckingMedicationByDrone.as_view(),name='medication_check'),

]+ static(settings.MEDIA_URL,document_root=settings.MEDIA_ROOT)
