# Musala test

## Introduction
API for Musala test.

## Technologies
- Python 3.6 or superior
- Django 3.2.9
- Django Rest Framework 3.12.4
- Celery 5.2.0
- Redis 3.5.3

## Steps to run project in local
1. Download project from develop branch
2. Create a virtual environment relative to inside project root. i.e: (python -m venv myvenv)
3. Activate virtual environment. i.e, for Windows: (myvenv\Scrips\active.bat)
4. Install project dependencies. (pip install -r requirements.txt)
5. Run server (python manage runserver)
## Steps to run Celery
1. Install redis (wget http://download.redis.io/redis-stable.tar.gz )
2. tar xzf redis-stable.tar.gz  
3. cd redis-stable  
4. sudo make install
5. After completing the make operation, we start the Redis server as a demon and test it.
6. redis-server --daemonize yes  
7. redis-cli ping
8. Run in console
9. celery -A musala beat -l info --logfile=celery.beat.log --detach
10. celery -A musala worker -l info --logfile=celery.log --detach
11. Check files celery.beat.log and celery.log
## API DOC
1. Go to api/doc/
## Run test
1. python manage.py test
## Examples 
1. registering a drone -> /api/drone/register/
2. loading a drone with medication items -> /api/drone/load/
3. checking loaded medication items for a given drone ->/api/medication/checkbydrone/?drone=1
4. checking available drones for loading -> /api/drone/available/
5. check drone battery level for a given drone -> /api/drone/checkbatery/1

## Django Admin
1. user : admin
2. pass :admin

